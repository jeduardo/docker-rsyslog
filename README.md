# rsyslog docker image

This is an **unofficial** image for rsyslog inspired by the 
[nginx docker image](https://github.com/nginxinc/docker-nginx/).

This image is intended to be used as a log collector and forwarder. It also has
the TCP and UDP inputs enabled. It also comes with the [Redis](https://redis.io/)
output enabled by default.

## Configuring

Configuration of the underlying image is made through environment variables,
listed below:

- `RSYSLOG_DOCKER_DEBUG`: Whether the container will display debug information
    in the beginning, such as the content of the processed configuration
    templates. Set to `true` to activate it. `false`.

- `RSYSLOG_DOCKER_INPUT_TCP_PORT`: Port the TCP syslog collector will expect to
    receive logs. Default value is `514`.

- `RSYSLOG_DOCKER_INPUT_UDP_PORT`: Port the UDP syslog collector will expect to
    receive logs. Default value is `514`.

- `RSYSLOG_DOCKER_OUTPUT_STDOUT`: Whether the container will output the
    collected logs to the container's standard output (`stdout`). Default is
    `true`, to allow seeing some activity when initially running the container.

- `RSYSLOG_DOCKER_OUTPUT_REDIS_MODE`: It supports the operation modes defined in
    the [omredis documentation](https://www.rsyslog.com/doc/master/configuration/modules/omhiredis.html).
    The default value is `publish`. 

- `RSYSLOG_DOCKER_OUTPUT_REDIS_HOST`: Hostname of the target Redis server.
    Default value is `redis`.

- `RSYSLOG_DOCKER_OUTPUT_REDIS_PORT`: Port of the remote Redis server. Default
    value is `6379`.

- `RSYSLOG_DOCKER_OUTPUT_REDIS_KEY`: Name of the Redis destination key. In the
    case of the `publish` mode, this is the channel for a collector to
    subscribe. Default value is `syslog`.

All defaults are defined under `templates/env.defaults`.

## Running

A Docker Compose file is provided to assist in running the image. Run `docker
compose up` to run it on the foreground or `docker compose up -d` to run it in
the background.

## Testing

First run `docker compose up` to get the system running.

Then, in another terminal, run `docker compose exec -ti redis redis-cli`. In the
following Redis prompt, run `subscribe syslog`.

Finally, go to the test directory with `cd test` and run `make test`.

The output should be similar to the one below:

```ShellSession
$ make test
logger -n 127.0.0.1 -P 5514 --tcp -t test-app -p emerg 'test TCP message from console'
logger -n 127.0.0.1 -P 5514 --udp -t test-app -p emerg 'test UDP message from console'
```

The `redis-cli` prompt should show:

```ShellSession
$ docker compose exec -ti redis redis-cli
127.0.0.1:6379> subscribe syslog
Reading messages... (press Ctrl-C to quit)
1) "subscribe"
2) "syslog"
3) (integer) 1
1) "message"
2) "syslog"
3) "<8>2023-02-05T21:06:04.303286+01:00 surface test-app test TCP message from console"
1) "message"
2) "syslog"
3) "<8>2023-02-05T21:06:04.316585+01:00 surface test-app test UDP message from console"
```

The `container logs` should show something similar to:

```ShellSession
...
docker-rsyslog-rsyslog-1  | Feb  5 21:08:42 my-laptop test-app test UDP message from console
docker-rsyslog-rsyslog-1  | Feb  5 21:08:42 my-laptop test-app test TCP message from console
```

## Extending

All templates files under `templates/*.template` need to be created with the
`rsyslog` configuration syntax. 

They are separated into: 

- `templates/10-global.template`: global configuration and module loading

- `templates/20-templates.template`: message template definitions

- `templates/30-outputs.template` configuration for different outputs

- `templates/40-rules.template` to store message routing rules. These files are
organized based on rsyslog configuration best practices.

Any new variables inserted need to be defined under `templates/env.defaults`
and added as a commented reference in the `docker-compose.yml` file.

## References

* [Understanding the /etc/rsyslog.conf file for configuring System Logging](https://www.thegeekdiary.com/understanding-the-etc-rsyslog-conf-file-for-configuring-system-logging/)
* [rsyslog.conf(5)](https://man7.org/linux/man-pages/man5/rsyslog.conf.5.html)
* [How to set variables in rsyslog v7](https://www.rsyslog.com/how-to-set-variables-in-rsyslog-v7/)
* [Advanced rsyslog configuration](https://linux-help.org/wiki/logging/rsyslog/advanced-rsyslog)
* [omredis documentation](https://www.rsyslog.com/doc/master/configuration/modules/omhiredis.html)
* [RainerScript: Conditionals](https://rsyslog-mm.readthedocs.io/en/v7.4_stable/config/conditionals.html)

