#!/bin/bash

source "/etc/rsyslog/templates/env.defaults"

for template in /etc/rsyslog/templates/*.template; do
  target=$(echo $template | sed 's/\/templates/\.d/' | sed 's/template$/conf/')
  cat ${template} | envsubst "$(env | sed 's/^/\$/' | tr '\n' ' ')" > $target
  echo "Processed ${template} into ${target}"
done
