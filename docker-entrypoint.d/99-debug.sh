#!/bin/sh

if [ "x${RSYSLOG_DOCKER_DEBUG}" != "xtrue" ]; then
  echo "Debug not enabled"
  return 0
fi

echo "Debug is enabled, showing configuration content"

echo "##### /etc/rsyslog.conf"
cat -n /etc/rsyslog.conf

for conf in /etc/rsyslog.d/*; do
  echo "##### ${conf}"
  cat -n ${conf}
done

echo "Debug is done"
