#!/bin/sh

NAME=$(echo $0 | sed 's/.sh$//' | sed 's/^\///')

echo $NAME started

for script in /docker-entrypoint.d/*; do
  ./${script}
done

echo $NAME finished

# Execute whatever command is passed to the container.
exec "$@"
