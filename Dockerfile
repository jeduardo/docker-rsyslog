FROM debian:bullseye-slim

LABEL maintainer="J. Eduardo <j.eduardo@gmail.com>"

# Install packages
RUN apt-get update \
    && apt-get install -y coreutils gettext-base \
    rsyslog \
    rsyslog-hiredis \
    && apt-get clean

# Created management directories for image
RUN mkdir /docker-entrypoint.d/ \
    && mkdir -p /etc/rsyslog/templates/ \
    && mkdir -p /etc/rsyslog.d/

COPY docker-entrypoint.sh /
COPY docker-entrypoint.d/* /docker-entrypoint.d/

COPY templates/* /etc/rsyslog/templates/
COPY conf/rsyslog.conf /etc/rsyslog.conf

ENTRYPOINT ["/docker-entrypoint.sh"]

EXPOSE 514

CMD ["rsyslogd", "-n"]
